// Перечислим с каких урлов мы хотим проксировать запрос
const PROXY_URLS = [
  '/api.test/*',
];

// Опишем дефолтный конфиг
const PROXY_CONFIG = {
  target: 'https://slack.com/api',
  changeOrigin: true,
  secure: false,
  logLevel: 'debug',
}

module.exports = {
  ...PROXY_URLS.reduce(
    (proxies, url) =>
      Object.assign(proxies, {
        [url]: PROXY_CONFIG,
      }),
    {},
  ),
};
