import { Injectable } from '@angular/core';

import { ApiService, Query } from 'core-library';
import { Observable } from 'rxjs';

@Injectable()
export class TestService {
  private readonly query: Query;

  constructor(private api: ApiService) {
    this.query = this.api.query('api.test');
  }

  getTest(): Observable<any> {
    return this.query('');
  }
}
