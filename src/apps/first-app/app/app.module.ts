import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { UiLibExampleModule } from 'ui-lib-example';
import { IconsLibraryModule, IconsRegistryService } from 'assets-library';
import { svgIconWork } from 'assets-library/icons';
import { ApiModule } from 'core-library';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TestService } from './test.service';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,

    UiLibExampleModule,
    IconsLibraryModule,
    ApiModule,
  ],
  providers: [TestService],
  bootstrap: [AppComponent],
})
export class AppModule {
  constructor(private svgIconRegistry: IconsRegistryService) {
    svgIconRegistry.registerIcons([svgIconWork]);
  }
}
