import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';

import { TestService } from './test.service';
import { take } from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AppComponent implements OnInit {
  title = 'angular-libs-examples';

  constructor(private service: TestService) { }

  ngOnInit(): void {
    this.service.getTest().pipe(take(1))
      .subscribe(res => console.log('Result from app', res));
  }
}
