import { NgModule } from '@angular/core';

import { SecondAppLibraryModule } from 'second-app-library';

import { TestComponent } from './test.component';
import { TestRoutingModule } from './test-routing.module';

@NgModule({
  declarations: [
    TestComponent,
  ],
  imports: [
    SecondAppLibraryModule,
    TestRoutingModule,
  ],
})
export class TestModule { }
