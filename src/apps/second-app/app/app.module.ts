import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { ApiModule } from 'core-library';
import { UiLibExampleModule } from 'ui-lib-example';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ApiModule,

    UiLibExampleModule,
  ],
  bootstrap: [AppComponent],
})
export class AppModule { }
