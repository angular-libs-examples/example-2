import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'lib-second-app-library',
  template: `
    <p>
      second-app-library works!
    </p>
  `,
  styles: [
  ]
})
export class SecondAppLibraryComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
