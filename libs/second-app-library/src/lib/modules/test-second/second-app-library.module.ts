import { NgModule } from '@angular/core';
import { SecondAppLibraryComponent } from './component';

@NgModule({
  declarations: [
    SecondAppLibraryComponent,
  ],
  imports: [
  ],
  exports: [
    SecondAppLibraryComponent,
  ],
})
export class SecondAppLibraryModule { }
