import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { getSum } from 'ui-lib-example/src/lib/utils';

import { TestService } from '../test.service';
import { take } from 'rxjs/operators';

@Component({
  selector: 'lib-ui-lib-example',
  templateUrl: './ui-lib-example.component.html',
  styleUrls: ['./ui-lib-example.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UiLibExampleComponent implements OnInit {
  form: FormGroup;

  private name = 'Ricardo Milos';

  constructor(
    private fb: FormBuilder,
    private service: TestService,
  ) {
    console.warn('Result of method from another chunk', getSum(4, 4));

    this.form = this.fb.group({
      name: [this.name, Validators.required],
    });
  }

  ngOnInit(): void {
    this.service.getTest().pipe(take(1))
      .subscribe(res => console.log('Result from lib', res));
  }
}
