import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';

import { CommonLibraryModule } from 'core-library';
import { IconsLibraryModule } from 'assets-library';

import { UiLibExampleComponent } from './component';
import { TestService } from './test.service';

@NgModule({
  declarations: [
    UiLibExampleComponent,
  ],
  imports: [
    IconsLibraryModule,
    CommonLibraryModule,
  ],
  providers: [
    TestService,
  ],
  exports: [
    UiLibExampleComponent,
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class UiLibExampleModule { }
