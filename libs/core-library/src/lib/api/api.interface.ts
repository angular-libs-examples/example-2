import { HttpHeaders } from '@angular/common/http';

import { Observable } from 'rxjs';

export type HttpMethod =
  'get' | 'post' | 'put' | 'delete' | 'jsonp' | 'options' | 'patch' | 'head' |
  'GET' | 'POST' | 'PUT' | 'DELETE' | 'JSONP' | 'OPTIONS' | 'PATCH' | 'HEAD';

export type ResponseType = 'arraybuffer' | 'json' | 'blob' | 'text' | undefined;

export interface ApiConfig {
  method?: HttpMethod;
  params?: { [key: string]: any };
  body?: { [key: string]: any };
  headers?: HttpHeaders | {
    [header: string]: string | string[];
  };
  responseType?: ResponseType;
}

export type Query = <T = any>(action?: string, config?: ApiConfig) => Observable<T>;
