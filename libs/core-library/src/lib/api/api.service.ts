import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';

import { stripRight } from './utils';
import { Query } from './api.interface';

@Injectable()
export class ApiService {
  constructor(private http: HttpClient) { }

  query(baseUrl: string): Query {
    return (
      action = '',
      {
        params = {},
        method = 'GET',
        headers = {},
        body = {},
        responseType = 'json',
      } = {},
    ): Observable<any> => {
      const apiUrl = stripRight('/', [baseUrl, action].join('/'))
        .replace('/?', '?');

      return this.http.request(method, apiUrl, { headers, params, body, responseType });
    };
  }
}
