import { Inject, Injectable, LOCALE_ID } from '@angular/core';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';

import { Observable } from 'rxjs';


@Injectable()
export class RequestInterceptor implements HttpInterceptor {
  constructor(@Inject(LOCALE_ID) public locale: string) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const update: any = {};
    update.headers = req.headers
      .append('Cache-Control', 'no-cache, no-store');

    return next.handle(req.clone(update));
  }
}
