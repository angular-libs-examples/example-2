export const stripRight = (sym: string, source: string): string =>
  source.substr(-1) === sym.toString()
    ? source.substr(0, source.length - 1)
    : source.toString();
