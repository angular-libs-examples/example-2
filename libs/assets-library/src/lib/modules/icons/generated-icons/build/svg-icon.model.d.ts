export declare type svgIcon = 'settings' | 'work';
export interface SvgIcon {
    name: svgIcon;
    data: string;
}
