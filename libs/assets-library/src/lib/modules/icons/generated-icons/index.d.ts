import { SvgIcon } from './build/svg-icon.model';
export declare type SvgIconNameSubset<T extends Readonly<SvgIcon[]>> = T[number]['name'];
export * from './build/svgIcon-settings.icon';
export * from './build/svgIcon-work.icon';
export * from './build/svg-icon.model';
