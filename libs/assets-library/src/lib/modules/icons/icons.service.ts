import { Injectable } from '@angular/core';
import { SvgIcon } from './generated-icons';

@Injectable({ providedIn: 'root' })
export class IconsRegistryService {
  private registry = new Map<string, string>();

  registerIcons(icons: SvgIcon[]): void {
    icons.forEach((icon: SvgIcon) => this.registry.set(icon.name, icon.data));
  }

  getIcon(iconName: string): string | undefined {
    if (!this.registry.has(iconName)) {
      // eslint-disable-next-line no-console
      console.warn(
        `We could not find the icon with name ${ iconName }, did you add it to the icon registry?`,
      );
    }

    return this.registry.get(iconName);
  }
}
