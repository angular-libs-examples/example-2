export const objConverting = ([name, data]: [string, string]) => ({ name, data });

export const iconsToArray = (obj: { [key: string]: string }): { name: string, data: string }[] =>
  Object.entries(obj).map(objConverting);
