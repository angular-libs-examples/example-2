import {
  ChangeDetectionStrategy, Component, ElementRef, Inject, Input, Optional,
} from '@angular/core';
import { DOCUMENT } from '@angular/common';

import { svgIcon } from './generated-icons';

import { IconsRegistryService } from './icons.service';

@Component({
  selector: 'lib-svg-icons',
  template: '<ng-content></ng-content>',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class IconsComponent {
  private svgIcon: SVGElement | undefined;

  @Input() set name(iconName: svgIcon) {
    const svgData = this.svgIconRegistry.getIcon(iconName);

    if (this.svgIcon) {
      this.el.nativeElement.removeChild(this.svgIcon);
    }

    if (svgData) {
      this.svgIcon = this.svgElementFromString(svgData);
      this.el.nativeElement.appendChild(this.svgIcon);
    }
  }

  constructor(
    private el: ElementRef,
    private svgIconRegistry: IconsRegistryService,
    @Optional() @Inject(DOCUMENT) private document: any,
  ) { }

  private svgElementFromString(svgContent: string): SVGElement {
    const div = this.document.createElement('DIV');
    div.innerHTML = svgContent;

    return div.querySelector('svg') || this.document.createElementNS('http://www.w3.org/200/svg', 'path');
  }
}
