import { NgModule } from '@angular/core';

import { SvgIconsModule } from '@ngneat/svg-icon';

import { iconsToArray } from './utils';
import icons from './svg-icons';
import { IconsComponent } from './icons.component';

@NgModule({
  declarations: [
    IconsComponent,
  ],
  imports: [
    SvgIconsModule.forRoot({
      icons: iconsToArray(icons),
    }),
  ],
  exports: [
    SvgIconsModule,
    IconsComponent,
  ],
})
export class IconsLibraryModule { }
